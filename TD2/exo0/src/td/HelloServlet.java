package td;

import javax.servlet.http.*;
import javax.servlet.*;
import javax.servlet.annotation.WebServlet;

import java.io.*;

@WebServlet("/HelloServlet")
public class HelloServlet extends HttpServlet{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		res.setContentType("text/html");//setting the content type 
		
		PrintWriter pw = res.getWriter(); //get the stream to write the data 
		
		//writing html in the stream
		pw.println("<html><body>");
		pw.println("Welcometoservlet");
		pw.println("</body></html>");
		
		pw.close();
		//closingthestream
	}
}